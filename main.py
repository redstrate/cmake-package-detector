import sys
import os
import subprocess
import shutil

project_path = sys.argv[1]
script_path = os.path.dirname(os.path.realpath(__file__))
cmake_path = script_path + "/cmake"

print(f"Location of our CMake modules: {cmake_path}")

print(f"Getting dependencies for {project_path}!")

print("Creating empty build directory...")

build_directory = project_path + "/cmake-dep-gen"

if not os.path.exists(build_directory):
    os.makedirs(build_directory)

print("Initializing CMake...")

os.chdir(build_directory)
subprocess.run(["cmake", f"-DCMAKE_MODULE_PATH={cmake_path}", ".."])

packages = open('packages.txt', 'r').read().split('\n')

os.chdir(script_path)

if os.path.exists(build_directory):
    shutil.rmtree(build_directory)

# begin post-processing the packages

# Remove PythonModules
packages[:] = [x for x in packages if "PythonModule" not in x]

# Remove QMLModules
packages[:] = [x for x in packages if "QMLModule" not in x]

# Remove other stuff that doesn't make sense or is on every system anyway
packages.remove('Threads')
packages.remove('WrapAtomic')
packages.remove('OpenGL')
packages.remove('WrapOpenGL')
packages.remove('Vulkan')

# Proprietary
packages.remove('Qt6QmlCompilerPlusPrivate')

print("CMake Packages:")
print('\n'.join(packages))